﻿namespace SuperSearch
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.FileListBox = new System.Windows.Forms.ListBox();
            this.OpenButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SearchDepthLabel = new System.Windows.Forms.Label();
            this.SearchDepthTextBox = new System.Windows.Forms.TextBox();
            this.FoldersOnlyCheckBox = new System.Windows.Forms.CheckBox();
            this.ContainsWordsCheckBox = new System.Windows.Forms.CheckBox();
            this.CreatedBeforeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CreatedAfterDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.FileObjectsOnlyCheckBox = new System.Windows.Forms.CheckBox();
            this.SearchFileContentCheckBox = new System.Windows.Forms.CheckBox();
            this.SearchPDFCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(24, 19);
            this.textBox1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1028, 31);
            this.textBox1.TabIndex = 0;
            // 
            // SearchButton
            // 
            this.SearchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchButton.Location = new System.Drawing.Point(906, 69);
            this.SearchButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(150, 44);
            this.SearchButton.TabIndex = 1;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // FileListBox
            // 
            this.FileListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileListBox.FormattingEnabled = true;
            this.FileListBox.ItemHeight = 37;
            this.FileListBox.Location = new System.Drawing.Point(24, 240);
            this.FileListBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.FileListBox.Name = "FileListBox";
            this.FileListBox.Size = new System.Drawing.Size(1028, 596);
            this.FileListBox.TabIndex = 2;
            // 
            // OpenButton
            // 
            this.OpenButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenButton.Location = new System.Drawing.Point(794, 875);
            this.OpenButton.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(262, 44);
            this.OpenButton.TabIndex = 3;
            this.OpenButton.Text = "Open File Location";
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(632, 875);
            this.button2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 44);
            this.button2.TabIndex = 4;
            this.button2.Text = "Open File";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // SearchDepthLabel
            // 
            this.SearchDepthLabel.AutoSize = true;
            this.SearchDepthLabel.Location = new System.Drawing.Point(24, 79);
            this.SearchDepthLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.SearchDepthLabel.Name = "SearchDepthLabel";
            this.SearchDepthLabel.Size = new System.Drawing.Size(155, 25);
            this.SearchDepthLabel.TabIndex = 5;
            this.SearchDepthLabel.Text = "Search Depth :";
            // 
            // SearchDepthTextBox
            // 
            this.SearchDepthTextBox.Location = new System.Drawing.Point(188, 75);
            this.SearchDepthTextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SearchDepthTextBox.Name = "SearchDepthTextBox";
            this.SearchDepthTextBox.Size = new System.Drawing.Size(104, 31);
            this.SearchDepthTextBox.TabIndex = 6;
            this.SearchDepthTextBox.Text = "4";
            // 
            // FoldersOnlyCheckBox
            // 
            this.FoldersOnlyCheckBox.AutoSize = true;
            this.FoldersOnlyCheckBox.Location = new System.Drawing.Point(502, 77);
            this.FoldersOnlyCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.FoldersOnlyCheckBox.Name = "FoldersOnlyCheckBox";
            this.FoldersOnlyCheckBox.Size = new System.Drawing.Size(166, 29);
            this.FoldersOnlyCheckBox.TabIndex = 7;
            this.FoldersOnlyCheckBox.Text = "Folders Only";
            this.FoldersOnlyCheckBox.UseVisualStyleBackColor = true;
            this.FoldersOnlyCheckBox.CheckedChanged += new System.EventHandler(this.FilesOnlyCheckBox_CheckedChanged);
            // 
            // ContainsWordsCheckBox
            // 
            this.ContainsWordsCheckBox.AutoSize = true;
            this.ContainsWordsCheckBox.Checked = true;
            this.ContainsWordsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ContainsWordsCheckBox.Location = new System.Drawing.Point(682, 77);
            this.ContainsWordsCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ContainsWordsCheckBox.Name = "ContainsWordsCheckBox";
            this.ContainsWordsCheckBox.Size = new System.Drawing.Size(202, 29);
            this.ContainsWordsCheckBox.TabIndex = 8;
            this.ContainsWordsCheckBox.Text = "Not Exact Match";
            this.ContainsWordsCheckBox.UseVisualStyleBackColor = true;
            this.ContainsWordsCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // CreatedBeforeDateTimePicker
            // 
            this.CreatedBeforeDateTimePicker.Location = new System.Drawing.Point(252, 125);
            this.CreatedBeforeDateTimePicker.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.CreatedBeforeDateTimePicker.Name = "CreatedBeforeDateTimePicker";
            this.CreatedBeforeDateTimePicker.Size = new System.Drawing.Size(358, 31);
            this.CreatedBeforeDateTimePicker.TabIndex = 9;
            this.CreatedBeforeDateTimePicker.Value = new System.DateTime(2049, 1, 5, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 125);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 36);
            this.label1.TabIndex = 10;
            this.label1.Text = "Created Before:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 175);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(198, 36);
            this.label2.TabIndex = 12;
            this.label2.Text = "Created After:";
            // 
            // CreatedAfterDateTimePicker
            // 
            this.CreatedAfterDateTimePicker.Location = new System.Drawing.Point(224, 175);
            this.CreatedAfterDateTimePicker.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.CreatedAfterDateTimePicker.Name = "CreatedAfterDateTimePicker";
            this.CreatedAfterDateTimePicker.Size = new System.Drawing.Size(358, 31);
            this.CreatedAfterDateTimePicker.TabIndex = 11;
            this.CreatedAfterDateTimePicker.Value = new System.DateTime(1997, 1, 5, 0, 0, 0, 0);
            // 
            // FileObjectsOnlyCheckBox
            // 
            this.FileObjectsOnlyCheckBox.AutoSize = true;
            this.FileObjectsOnlyCheckBox.Location = new System.Drawing.Point(348, 77);
            this.FileObjectsOnlyCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.FileObjectsOnlyCheckBox.Name = "FileObjectsOnlyCheckBox";
            this.FileObjectsOnlyCheckBox.Size = new System.Drawing.Size(140, 29);
            this.FileObjectsOnlyCheckBox.TabIndex = 13;
            this.FileObjectsOnlyCheckBox.Text = "Files Only";
            this.FileObjectsOnlyCheckBox.UseVisualStyleBackColor = true;
            this.FileObjectsOnlyCheckBox.CheckedChanged += new System.EventHandler(this.FileObjectsOnlyCheckBox_CheckedChanged);
            // 
            // SearchFileContentCheckBox
            // 
            this.SearchFileContentCheckBox.AutoSize = true;
            this.SearchFileContentCheckBox.Location = new System.Drawing.Point(682, 177);
            this.SearchFileContentCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SearchFileContentCheckBox.Name = "SearchFileContentCheckBox";
            this.SearchFileContentCheckBox.Size = new System.Drawing.Size(261, 29);
            this.SearchFileContentCheckBox.TabIndex = 14;
            this.SearchFileContentCheckBox.Text = "Search Word Contents";
            this.SearchFileContentCheckBox.UseVisualStyleBackColor = true;
            this.SearchFileContentCheckBox.CheckedChanged += new System.EventHandler(this.SearchFileContentCheckBox_CheckedChanged);
            // 
            // SearchPDFCheckBox
            // 
            this.SearchPDFCheckBox.AutoSize = true;
            this.SearchPDFCheckBox.Location = new System.Drawing.Point(682, 133);
            this.SearchPDFCheckBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.SearchPDFCheckBox.Name = "SearchPDFCheckBox";
            this.SearchPDFCheckBox.Size = new System.Drawing.Size(252, 29);
            this.SearchPDFCheckBox.TabIndex = 15;
            this.SearchPDFCheckBox.Text = "Search PDF Contents";
            this.SearchPDFCheckBox.UseVisualStyleBackColor = true;
            this.SearchPDFCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1080, 938);
            this.Controls.Add(this.SearchPDFCheckBox);
            this.Controls.Add(this.SearchFileContentCheckBox);
            this.Controls.Add(this.FileObjectsOnlyCheckBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CreatedAfterDateTimePicker);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CreatedBeforeDateTimePicker);
            this.Controls.Add(this.ContainsWordsCheckBox);
            this.Controls.Add(this.FoldersOnlyCheckBox);
            this.Controls.Add(this.SearchDepthTextBox);
            this.Controls.Add(this.SearchDepthLabel);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.OpenButton);
            this.Controls.Add(this.FileListBox);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "Main_Form";
            this.ShowIcon = false;
            this.Text = "Super Search v1.4 - Created By John Dodd";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.ListBox FileListBox;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label SearchDepthLabel;
        private System.Windows.Forms.TextBox SearchDepthTextBox;
        private System.Windows.Forms.CheckBox FoldersOnlyCheckBox;
        private System.Windows.Forms.CheckBox ContainsWordsCheckBox;
        private System.Windows.Forms.DateTimePicker CreatedBeforeDateTimePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker CreatedAfterDateTimePicker;
        private System.Windows.Forms.CheckBox FileObjectsOnlyCheckBox;
        private System.Windows.Forms.CheckBox SearchFileContentCheckBox;
        private System.Windows.Forms.CheckBox SearchPDFCheckBox;
    }
}

