﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices.ComTypes;
using System.Xml;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.parser;
using Microsoft.Office.Interop;
using Microsoft.Office.Core;
using org.apache.tika.metadata;

namespace SuperSearch
{
    public partial class Main_Form : Form
    {

        public static List<String> FilesFound = new List<string>();

        public static bool SearchOnlyFolders = false;

        public static bool ContainsWords = true; //starts checked

        public static bool FileObjectsOnly = false;

        public static bool SearchWordDocContentsBool = false;

        public static bool SearchPDF = false;

        public static string LastPath = "n:\\";

        public Main_Form()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();

            //try for n drive
            try
            {
                FBD.SelectedPath = LastPath;
            }
            //otherwise search c drive
            catch
            {
                FBD.SelectedPath = "c:\\";
            }


            if (FBD.ShowDialog() == DialogResult.OK && textBox1.Text != "")
            {
                LastPath = FBD.SelectedPath;

                MessageBox.Show("Beginning Search Of: "+FBD.SelectedPath+"\nFor: "+textBox1.Text+"\n\nYou will be alerted when the search is complete.");

                //clear files found
                FilesFound.Clear();
                FileListBox.Items.Clear();

                //minimize before searching
                this.WindowState = FormWindowState.Minimized;

                //default depth of 15 MAx Search Depth acronym
                int MSD = 15;

                Int32.TryParse(SearchDepthTextBox.Text,out MSD);

                if (ContainsWords == true)
                {
                    //begin search for all keywords
                    string[] terms = textBox1.Text.Split();
                    List<string> SearchTermList = new List<string>();
                    SearchTermList.AddRange(terms);

                    for (int i = 0; i < SearchTermList.Count; i++)
                    {
                        SearchTermList[i] = SearchTermList[i].Replace(" ","");
                    }

                    //do actual search
                    SearchFolderKeyWords(FBD.SelectedPath, SearchTermList, 0, MSD);
                }
                else
                {
                    //begin search for exact phrase!
                    SearchFolder(FBD.SelectedPath, textBox1.Text, 0, MSD);
                }

                //add the stuff to the list and check the dates to comply with user query
                AddToListAndCheckDate();

                //normalize window on search complete...
                this.WindowState = FormWindowState.Normal;
                this.BringToFront(); //bring form to front
                MessageBox.Show("Search Complete "+FileListBox.Items.Count+" Results Found");
            }

        }

        public static void SearchFolder(string DirectoryName, string SearchTerm, int SearchDepth = 0, int SearchDepthMax = 15)
        {
            //if the search isn't yet deep enough
            if (SearchDepth <= SearchDepthMax)
            {
         
                try
                {
                    //if search only folders is false
                    if (SearchOnlyFolders == false)
                    {
                        //get a list of files in the directoyr
                        string[] FilesWithin = Directory.GetFiles(DirectoryName);
                        foreach (string FileName in FilesWithin)
                        {
                            try
                            {
                                //term to search!
                                string Searchee = FileName.Replace(DirectoryName, "");

                                //if filename contains term
                                if (Searchee.ToLower().Contains(SearchTerm.ToLower()))
                                {
                                    FilesFound.Add(FileName);
                                }

                                if (SearchWordDocContentsBool == true)
                                {
                                    //its a word doc :D
                                    if (FileName.ToLower().Contains(".docx"))
                                    {
                                        string text = GetDocText(FileName);

                                        text = CleanWordText(text);

                                        if (text.ToLower().Contains(SearchTerm.ToLower()))
                                        {
                                            FilesFound.Add(FileName);
                                        }
                                    }
                                }

                                if (SearchPDF == true)
                                {
                                    //its a pdf :D
                                    if (FileName.ToLower().Contains(".pdf"))
                                    {
                                        string text = GetPDFTextFast(FileName);

                                        if (text.ToLower().Contains(SearchTerm.ToLower()))
                                        {
                                            FilesFound.Add(FileName);
                                        }
                                    }
                                }
                            }
                            catch
                            {
                                //nothing...
                            }
                        }
                    }
                    
                    //search subdirectories as well
                    string[] SubDirs = Directory.GetDirectories(DirectoryName);
                    foreach (string SubDir in SubDirs)
                    {
                        try
                        {
                            //term to search!
                            string Searchee = SubDir.Replace(DirectoryName, "");

                            //if subdirectory contains term...
                            if (Searchee.ToLower().Contains(SearchTerm.ToLower()))
                            {
                                FilesFound.Add(SubDir);
                            }

                            //search subfolders, increase search depth by one!
                            SearchFolder(SubDir, SearchTerm, SearchDepth + 1, SearchDepthMax);
                        }
                        catch
                        {
                            //nothing...
                        }
                    }

                }
                catch
                {
                    //nothin
                }

            }
        }

        public static string CleanWordText(string a)
        {
            string returner = "";
            for(int i = 0; i < a.Length; i++)
            {
                if (a[i] == '<')
                {
                    string remove = "";
                    int it = i;
                    while(it < a.Length)
                    {
                        remove += a[it];
                        it++;

                        if (remove.Contains(">"))
                        {
                            a = a.Replace(remove,"");

                            if (a.Contains("<"))
                            {
                                //recursion!
                                a = CleanWordText(a);
                                returner = a;
                                return returner;
                            }
                            else
                            {
                                returner = a;
                                return returner;
                            }
                        }
                    }
                }
            }

            returner = a;
            return returner;
        }

        public static void SearchFolderKeyWords(string DirectoryName, List<string> SearchTerms, int SearchDepth = 0, int SearchDepthMax = 15)
        {
            //if the search isn't yet deep enough
            if (SearchDepth <= SearchDepthMax)
            {

                try
                {
                    //if search only folders is false
                    if (SearchOnlyFolders == false)
                    {
                        //get a list of files in the directoyr
                        string[] FilesWithin = Directory.GetFiles(DirectoryName);
                        foreach (string FileName in FilesWithin)
                        {
                            //term to search!
                            string Searchee = FileName;
                            //term to search!
                            Searchee = FileName.Replace(DirectoryName, "");

                            bool ContainsAll = true;
                            foreach (string SearchTerm in SearchTerms)
                            {
                                //if filename contains term
                                if (!Searchee.ToLower().Contains(SearchTerm.ToLower()))
                                {
                                    ContainsAll = false;
                                }
                            }

                            //if it has all search terms then add it
                            if(ContainsAll == true)
                            {
                                FilesFound.Add(FileName);
                            }

                        }
                    }

                    //search subdirectories as well
                    string[] SubDirs = Directory.GetDirectories(DirectoryName);
                    foreach (string SubDir in SubDirs)
                    {
                        //term to search!
                        string Searchee = SubDir;
                        //term to search!
                        //term to search!
                        Searchee = SubDir.Replace(DirectoryName, "");


                        bool ContainsAll = true;
                        foreach (string SearchTerm in SearchTerms)
                        {
                            //if subdirectory contains term...
                            if (!Searchee.ToLower().Contains(SearchTerm.ToLower()))
                            {
                                ContainsAll = false;
                            }
                        }

                        if (ContainsAll == true)
                        {
                            FilesFound.Add(SubDir);
                        }

                        //search subfolders, increase search depth by one!
                        SearchFolderKeyWords(SubDir, SearchTerms, SearchDepth + 1, SearchDepthMax);
                        
                    }

                }
                catch
                {
                    //nothin
                }

            }
        }

        private void AddToListAndCheckDate()
        {
            
            foreach (string FN in FilesFound)
            {
                //check if file is before before range
                //and after after range...

                //compare instructions...
                //https://msdn.microsoft.com/en-us/library/system.datetime.compare(v=vs.110).aspx

                
                //to compare...
                DateTime CreationTime = DateTime.Now; //set default to now...

                //if it exists...
                if (Directory.Exists(FN))
                {
                    CreationTime = Directory.GetCreationTime(FN).Date;
                }
                else if (File.Exists(FN))
                {
                    CreationTime = File.GetCreationTime(FN).Date;
                }

                //now this is pod racing!
                if (Directory.Exists(FN) || File.Exists(FN))
                {
                    if (DateTime.Compare(CreationTime.Date, CreatedAfterDateTimePicker.Value.Date) > 0)
                    {
                        //t1 is later than t2 and...
                        if (DateTime.Compare(CreationTime.Date, CreatedBeforeDateTimePicker.Value.Date) < 0)
                        {
                            //t1 is earlier than t2
                            FileListBox.Items.Add(FN);
                        }
                    }
                    else if (DateTime.Compare(CreationTime.Date, CreatedBeforeDateTimePicker.Value.Date) == 0)
                    {
                        //add it just in case...
                        FileListBox.Items.Add(FN);
                    }
                }

                //if it was a directory...
                if (FileObjectsOnly == true && Directory.Exists(FN))
                {
                    FileListBox.Items.Remove(FN);
                }

            }
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            try
            {
                string OpenThis = FileListBox.SelectedItem.ToString();
                string SpecificFile = "";


                while (OpenThis[OpenThis.Length - 1] != '\\')
                {
                    string temp = "";
                    //trim last char
                    SpecificFile += OpenThis[OpenThis.Length - 1];
                    temp = OpenThis.Substring(0, (OpenThis.Length - 1));
                    OpenThis = temp;
                }

                try
                {
                    Process.Start("explorer.exe", OpenThis);
                }
                catch
                {
                    MessageBox.Show("File Not Found!");
                }

            }
            catch
            {
                //nothing
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            try
            {
                Process.Start("explorer.exe", FileListBox.SelectedItem.ToString());
            }
            catch
            {
                MessageBox.Show("File Not Found!");
            }
        }

        private void FilesOnlyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FoldersOnlyCheckBox.Checked == true)
            {
                SearchOnlyFolders = true;
            }
            else
            {
                SearchOnlyFolders = false;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (ContainsWordsCheckBox.Checked == true)
            {
                ContainsWords = true;
                SearchFileContentCheckBox.Checked = false;
                SearchPDFCheckBox.Checked = false;
            }
            else
            {
                ContainsWords = false;
            }
        }

        private void FileObjectsOnlyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FileObjectsOnlyCheckBox.Checked == true)
            {
                FileObjectsOnly = true;
            }
            else
            {
                FileObjectsOnly = false;
            }
        }

        private static string GetDocText(string Path)
        {
            /*
            //http://www.c-sharpcorner.com/UploadFile/muralidharan.d/how-to-create-word-document-using-C-Sharp/

            object miss = System.Reflection.Missing.Value;
            object path = Path;
            object readOnly = true;
            Microsoft.Office.Interop.Word.Document docs = WordDocApp.Documents.Open(ref path, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);
            string AllText = "";
            for(int i = 0; i < docs.Paragraphs.Count; i++)
            {
                AllText += " \r\n " + docs.Paragraphs[i + 1].Range.Text.ToString();
            }

            docs.Close();
            return AllText;
            */
            
            //use filepath to open xml doc
            //WordprocessingDocument WPD = WordprocessingDocument.Open(Path, true);

            string AllText = null;

            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(Path,true))
            {
                AllText = null;
                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    AllText = sr.ReadToEnd();
                }
            }

            return AllText;

        }

        private static string GetPDFTextFast(string Path)
        {
            string returner = "";

            PdfReader PR = new PdfReader(Path);

            for(int page = 1; page <= PR.NumberOfPages; page++)
            {
                returner += PdfTextExtractor.GetTextFromPage(PR, page);   
            }

            PR.Dispose();

            return returner;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.AcceptButton = SearchButton;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void SearchFileContentCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (SearchFileContentCheckBox.Checked == true)
            {
                SearchWordDocContentsBool = true;

               if (ContainsWordsCheckBox.Checked == true)
               {
                   ContainsWordsCheckBox.Checked = false;
               }
            }
            else
            {
                SearchWordDocContentsBool = false;
            }
        }

        //search pdf contents checkbox
        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if(SearchPDFCheckBox.Checked == true)
            {
                SearchPDF = true;
                ContainsWordsCheckBox.Checked = false;
                ContainsWords = false;
            }
            else
            {
                SearchPDF = false;
            }
        }

    }
}
